CREATE TABLE `transections` (
  `transection_id` varchar(100) NOT NULL,
  `hash` varchar(100) NOT NULL,
  `block_number` text NOT NULL,
  `from` varchar(100) NOT NULL,
  `to` varchar(100) NOT NULL,
  `value` text NOT NULL,
  `gas` int(11) NOT NULL,
  `gas_price` text NOT NULL,
  `nonce` int(11) NOT NULL,
  `action` varchar(50) NOT NULL COMMENT 'deposit,withdraw',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;