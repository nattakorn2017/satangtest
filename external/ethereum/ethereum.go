package ethereum

import (
	"context"
	"math/big"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	"gitlab.com/satang/app/transactions"
)

type EthereumClient struct {
	client *ethclient.Client
}

func NewEthereumClient(client *ethclient.Client) *EthereumClient {
	return &EthereumClient{client: client}
}

func (ec *EthereumClient) MonitorTransactions(address common.Address, blockNumberFrom, blockNumberTo uint64) ([]transactions.Transaction, error) {

	ctx := context.Background()
	var ts []transactions.Transaction

	for i := blockNumberFrom; i <= blockNumberTo; i++ {
		block, err := ec.client.BlockByNumber(ctx, big.NewInt(int64(i)))
		if err != nil {
			return ts, err
		}

		for _, tx := range block.Transactions() {

			if tx.To() == nil {
				continue
			}

			from, err := types.Sender(types.LatestSignerForChainID(tx.ChainId()), tx)
			if err != nil {
				return ts, err
			}

			if tx.To().Hex() == address.Hex() || from.Hex() == address.Hex() {
				ts = append(ts, transactions.Transaction{
					Hash:        tx.Hash(),
					BlockNumber: big.NewInt(int64(i)),
					From:        from.Hex(),
					To:          tx.To().Hex(),
					Value:       tx.Value(),
					Gas:         tx.Gas(),
					GasPrice:    tx.GasPrice(),
					Nonce:       tx.Nonce(),
				})
			}
		}
	}

	return ts, nil
}
