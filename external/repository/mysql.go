package repository

import (
	"log"

	"github.com/labstack/echo/v4"
	"gitlab.com/satang/app/transactions"
	"gorm.io/gorm"
)

type Mysql struct {
	db *gorm.DB
}

func NewMysql(db *gorm.DB) *Mysql {
	return &Mysql{db: db}
}

func (m *Mysql) Created(c echo.Context, tnx []transactions.TransactionEntity) error {
	if err := m.db.Debug().Create(&tnx).Error; err != nil {
		log.Printf("created transactions error is : %v", err)
		return err
	}
	return nil
}

func (m *Mysql) FindTransections(c echo.Context, colums []string, scopes []func(db *gorm.DB) *gorm.DB) ([]transactions.TransactionEntity, error) {
	var resp []transactions.TransactionEntity
	if err := m.db.Debug().Select(colums).Scopes(scopes...).Find(&resp).Error; err != nil {
		log.Printf("list transections error is : %v", err)
		return resp, err
	}
	return resp, nil
}
