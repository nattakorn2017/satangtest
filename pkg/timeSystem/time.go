package timeSystem

import "time"

type Date struct {
	now time.Time
}

func NewTime(now time.Time) *Date {
	return &Date{now: now}
}

func (t Date) TimeNow() time.Time {
	return t.now
}
