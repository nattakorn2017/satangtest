package config

import (
	"time"
)

var Config *RoadConfig

type RoadConfig struct {
	Serve    Runserve
	Database Mysql
	Eth      EthConfiguration
}

type Runserve struct {
	Port string
}

type Mysql struct {
	Username     string
	Password     string
	Host         string
	Port         string
	DBName       string
	MaxOpenConns int
	MaxIdleConns int
	MaxLifetime  time.Duration
}

type EthConfiguration struct {
	EthereumAPIURL string `json:"ethereumAPIURL"`
}
