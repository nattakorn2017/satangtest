package config

import (
	"log"
	"strings"

	"github.com/spf13/viper"
)

func Init() {
	initViper()
	loadConfig()
}

func initViper() {
	viper.AddConfigPath("config")
	viper.SetConfigName("config")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("connot read config in viper %v", err)
	}
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
}

func loadConfig() {
	Config = &RoadConfig{
		Serve: Runserve{
			Port: viper.GetString("SERVE.PORT"),
		},
		Database: Mysql{
			Username:     viper.GetString("DATABASE.USERNAME"),
			Password:     viper.GetString("DATABASE.PASSWORD"),
			Host:         viper.GetString("DATABASE.HOST"),
			Port:         viper.GetString("DATABASE.PORT"),
			DBName:       viper.GetString("DATABASE.DBNAME"),
			MaxOpenConns: viper.GetInt("DATABASE.MAX_OPEN_CONNS"),
			MaxIdleConns: viper.GetInt("DATABASE.MAX_IDLE_CONNS"),
			MaxLifetime:  viper.GetDuration("DATABASE.MAX_LIFETIME"),
		},
		Eth: EthConfiguration{
			EthereumAPIURL: viper.GetString("ETH.API_URL"),
		},
	}
}
