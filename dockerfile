FROM golang:1.17.3 as builder

WORKDIR /app

COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=386 go build -mod=vendor \
    -ldflags "-X main.githash=$(git rev-parse HEAD) -X main.buildstamp=$(date +%Y%m%d.%H%M%S)" \
    -o goapp cmd/main.go

FROM alpine:latest

WORKDIR /app

RUN apk update && apk add --no-cache \
    ca-certificates \
    tzdata

ENV TZ=Asia/Bangkok

COPY ./config ./config
COPY --from=builder /app/goapp ./goapp

EXPOSE 8080

CMD ["./goapp"]
