# satangTest

## Step to run

    1.register eth api from https://app.infura.io
    2.run docker-compose from ./docker/docker-compose.yml
    3.import sql database exec by localhost:8855 
        - serve: transactions_db
        - username: root
        - password: p@ssw0rd
        - database: transections
    3.import collection postman path ./psotman/satang.postman_collection.json to postman program
    4.run go program
        cmd: make run or go run cmd/main.go
    5.run postman to get transections eth

## 
