package main

import (
	"fmt"
	"log"
	"time"

	"runtime"

	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/labstack/echo/v4"
	echoMiddleWare "github.com/labstack/echo/v4/middleware"
	"gitlab.com/satang/app/route"
	"gitlab.com/satang/app/transactions"
	"gitlab.com/satang/config"
	"gitlab.com/satang/external/ethereum"
	"gitlab.com/satang/external/repository"
	"gitlab.com/satang/pkg/timeSystem"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func init() {
	runtime.GOMAXPROCS(1)
	config.Init()
}

func main() {
	// initail system
	now := time.Now()
	e := initFramework()
	db := initMysql(config.Config.Database)
	ethClient := initEthereumClient(config.Config.Eth.EthereumAPIURL)
	handle := initHandle(db, ethClient, now)
	// run serve
	app := route.NewRoute(e, handle)
	app.Route()

}

func initHandle(db *gorm.DB, ethClient *ethclient.Client, t time.Time) *transactions.HTTPHandler {
	return transactions.NewHTTPHandler(transactions.NewUsecase(repository.NewMysql(db), ethereum.NewEthereumClient(ethClient), timeSystem.NewTime(t)))
}

func initFramework() *echo.Echo {
	e := echo.New()
	e.Use(echoMiddleWare.CORSWithConfig(echoMiddleWare.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{"*"},
	}))

	e.Use(echoMiddleWare.Logger())
	e.Use(echoMiddleWare.Recover())

	return e
}

func initMysql(cfg config.Mysql) *gorm.DB {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=True", cfg.Username, cfg.Password, cfg.Host, cfg.Port, cfg.DBName)
	log.Printf("connecting to %s:[secret]@tcp(%s:%s)/%s?parseTime=True", cfg.Username, cfg.Host, cfg.Port, cfg.DBName)
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatalf("[x] cannot connect db [%s:%s/%s] error: %s", cfg.Host, cfg.Port, cfg.DBName, err)
	}
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalf("[x] cannot get sql db [%s:%s/%s] error: %s", cfg.Host, cfg.Port, cfg.DBName, err)
	}
	sqlDB.SetMaxOpenConns(cfg.MaxOpenConns)
	sqlDB.SetMaxIdleConns(cfg.MaxIdleConns)
	sqlDB.SetConnMaxLifetime(cfg.MaxLifetime)
	log.Printf("[√] connect to db: %s, host: %s", cfg.DBName, cfg.Host)

	return db
}

func initEthereumClient(ethereumAPIURL string) *ethclient.Client {
	client, err := ethclient.Dial(ethereumAPIURL)
	if err != nil {
		log.Fatalf("[x] cannot connect eth [%s] error: %s", ethereumAPIURL, err.Error())
		return nil
	}
	log.Printf("[√] connect eth [%s] success", ethereumAPIURL)
	return client
}
