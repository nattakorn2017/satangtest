package transactions

import "github.com/ethereum/go-ethereum/common"

type Request struct {
	Address    []string `json:"address"`
	BlockStart uint64   `json:"blockStart"`
	BlockEnd   uint64   `json:"blockEnd"`
}

type EthereumAddress struct {
	Address common.Address `json:"address"`
}
