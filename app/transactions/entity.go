package transactions

import (
	"log"
	"math/big"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type TransactionEntity struct {
	TransectionId string    `gorm:"transection_id"`
	Hash          string    `gorm:"hash"`
	BlockNumber   string    `gorm:"block_number"`
	From          string    `gorm:"from"`
	To            string    `gorm:"to"`
	Value         string    `gorm:"value"`
	Gas           uint64    `gorm:"gas"`
	GasPrice      string    `gorm:"gas_price"`
	Nonce         uint64    `gorm:"nonce"`
	Action        string    `gorm:"action"` // deposit,withdraw
	CreatedAt     time.Time `gorm:"created_at"`
}

func (TransactionEntity) TableName() string {
	return "transections"
}

func (e *TransactionEntity) BeforeCreate(tx *gorm.DB) (err error) {
	if e.TransectionId == "" {
		uuid, err := uuid.NewRandom()
		if err != nil {
			log.Printf("cannot generate random uuid : %v", err)
			return err
		}
		e.TransectionId = uuid.String()
	}
	return nil
}

type Transaction struct {
	Hash        common.Hash `json:"hash"`
	BlockNumber *big.Int    `json:"blockNumber"`
	From        string      `json:"from"`
	To          string      `json:"to"`
	Value       *big.Int    `json:"value"`
	Gas         uint64      `json:"gas"`
	GasPrice    *big.Int    `json:"gasPrice"`
	Nonce       uint64      `json:"nonce"`
}
