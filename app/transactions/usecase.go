package transactions

import (
	"github.com/ethereum/go-ethereum/common"
	"github.com/labstack/echo/v4"
)

type Usecase struct {
	mysql IMysql
	eth   IEthereumClient
	time  ITime
}

func NewUsecase(mysql IMysql, eth IEthereumClient, t ITime) *Usecase {
	return &Usecase{mysql: mysql, eth: eth, time: t}
}

func (u *Usecase) Usecases(c echo.Context, req Request) (map[string][]TransactionEntity, error) {

	now := u.time.TimeNow()
	var allTxEntity []TransactionEntity
	mapReturn := make(map[string][]TransactionEntity)
	for _, adrr := range req.Address {
		tnx, err := u.eth.MonitorTransactions(common.HexToAddress(adrr), req.BlockStart, req.BlockEnd)
		if err != nil {
			return mapReturn, err
		}
		for _, tx := range tnx {
			var action string
			if tx.From == adrr {
				action = "deposit"
			} else {
				action = "withdraw"
			}

			txEntity := TransactionEntity{
				Hash:        tx.Hash.String(),
				BlockNumber: tx.BlockNumber.String(),
				From:        tx.From,
				To:          tx.To,
				Value:       tx.Value.String(),
				Gas:         tx.Gas,
				GasPrice:    tx.GasPrice.String(),
				Nonce:       tx.Nonce,
				Action:      action,
				CreatedAt:   now,
			}
			allTxEntity = append(allTxEntity, txEntity)
			mapReturn[adrr] = append(mapReturn[adrr], txEntity)
		}
	}

	if err := u.mysql.Created(c, allTxEntity); err != nil {
		return mapReturn, err
	}

	return mapReturn, nil
}
