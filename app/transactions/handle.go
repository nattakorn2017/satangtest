package transactions

import (
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/satang/pkg/response"
)

type HTTPHandler struct {
	usecase IUsecase
}

func NewHTTPHandler(usecase IUsecase) *HTTPHandler {
	return &HTTPHandler{
		usecase: usecase,
	}
}

func (h HTTPHandler) GetTransections(c echo.Context) error {

	var req Request
	if errBind := c.Bind(&req); errBind != nil {
		log.Printf("error on func Bind req : %v", errBind.Error())
		return c.JSON(http.StatusOK, response.MakeHTTPFailedResponse("9999", "bind req error"))
	}

	tx, errUsecase := h.usecase.Usecases(c, req)
	if errUsecase != nil {
		log.Printf("error get beef : %v", errUsecase)
		return c.JSON(http.StatusOK, response.MakeHTTPFailedResponse("9999", "internal error"))
	}

	return c.JSON(http.StatusOK, response.MakeHTTPSuccessResponse(c, tx))
}
