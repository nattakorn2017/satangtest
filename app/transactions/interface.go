package transactions

import (
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
)

type IUsecase interface {
	Usecases(c echo.Context, req Request) (map[string][]TransactionEntity, error)
}

type IMysql interface {
	Created(c echo.Context, tnx []TransactionEntity) error
	FindTransections(c echo.Context, colums []string, scopes []func(db *gorm.DB) *gorm.DB) ([]TransactionEntity, error)
}

type IEthereumClient interface {
	MonitorTransactions(addresses common.Address, blockNumberFrom, blockNumberTo uint64) ([]Transaction, error)
}

type ITime interface {
	TimeNow() time.Time
}
