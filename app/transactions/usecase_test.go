package transactions_test

import (
	"math/big"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/golang/mock/gomock"
	"github.com/labstack/echo/v4"
	"gitlab.com/satang/app/transactions"
	"gitlab.com/satang/app/transactions/mock"
	"gopkg.in/go-playground/assert.v1"
)

func TestUsecase(t *testing.T) {

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mIMysql := mock.NewMockIMysql(ctrl)
	mIEth := mock.NewMockIEthereumClient(ctrl)
	mITime := mock.NewMockITime(ctrl)
	usecase := transactions.NewUsecase(mIMysql, mIEth, mITime)
	now := time.Now()

	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rec := httptest.NewRecorder()
	ctx := e.NewContext(req, rec)

	expected := map[string][]transactions.TransactionEntity{}
	expected["addr"] = []transactions.TransactionEntity{
		{
			Hash:        "0x0000000000000000000000000000000000000000000000000000000000000000",
			BlockNumber: "1",
			From:        "addr",
			To:          "to",
			Value:       "1",
			Gas:         1,
			GasPrice:    "1",
			Nonce:       1,
			Action:      "deposit",
			CreatedAt:   now,
		},
	}

	request := transactions.Request{
		Address:    []string{"addr"},
		BlockStart: 1,
		BlockEnd:   10,
	}

	mITime.EXPECT().TimeNow().Return(now)
	mIEth.EXPECT().MonitorTransactions(common.HexToAddress("addr"), request.BlockStart, request.BlockEnd).Return([]transactions.Transaction{
		{
			Hash:        common.HexToHash("hash"),
			BlockNumber: big.NewInt(1),
			From:        "addr",
			To:          "to",
			Value:       big.NewInt(1),
			Gas:         1,
			GasPrice:    big.NewInt(1),
			Nonce:       1,
		},
	}, nil)
	mIMysql.EXPECT().Created(ctx, []transactions.TransactionEntity{
		{
			Hash:        "0x0000000000000000000000000000000000000000000000000000000000000000",
			BlockNumber: "1",
			From:        "addr",
			To:          "to",
			Value:       "1",
			Gas:         1,
			GasPrice:    "1",
			Nonce:       1,
			Action:      "deposit",
			CreatedAt:   now,
		},
	}).Return(nil)

	result, _ := usecase.Usecases(ctx, request)
	assert.Equal(t, expected, result)

}
