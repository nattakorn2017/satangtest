package route

import (
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/satang/app/transactions"
	"gitlab.com/satang/config"
	"gitlab.com/satang/pkg/response"
)

type initailize struct {
	e      *echo.Echo
	handle *transactions.HTTPHandler
}

func NewRoute(
	e *echo.Echo,
	handle *transactions.HTTPHandler,
) *initailize {
	return &initailize{
		e:      e,
		handle: handle,
	}
}

func (i *initailize) Route() {

	// welcome message
	i.e.GET("/", func(c echo.Context) error {
		return c.JSON(http.StatusOK, response.MakeHTTPSuccessResponse(c, response.PingServe{Status: "OK", Message: "wellcome"}))
	})

	i.e.GET("/eth/transactions", i.handle.GetTransections)

	go func() {
		log.Printf("app listening port :%v", config.Config.Serve.Port)
		log.Fatal(i.e.Start(":" + config.Config.Serve.Port))
	}()
	gracefulShutdown(i.e)

}
